from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy import log
from scrapy.mail import MailSender

class SpiderOpenCloseLogging(object):

    def __init__(self):
        dispatcher.connect(self.spider_opened, signal=signals.spider_opened)
        dispatcher.connect(self.spider_closed, signal=signals.spider_closed)
        self.mailer = MailSender()
        self.mailer.smtphost = "smtp.qq.com"
        self.mailer.smtpuser = "admin@liuko.com"
        self.mailer.smtppass = "5824205858"
        self.mailer.mailfrom = "admin@liuko.com"

    def spider_opened(self, spider):
        log.msg("opened spider %s" % spider.name)
        self.mailer.send(to=["lqf800@qq.com"], subject="scrapy", body="scrapy start:"+str(spider.domain))

    def spider_closed(self, spider):
        self.mailer.send(to=["lqf800@qq.com"], subject="scrapy", body="scrapy close num:"+str(spider.amount_num)+"money:"+str(spider.amount_money)+"domain:"+str(spider.domain))
        log.msg("closed spider %s" % spider.name)

